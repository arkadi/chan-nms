#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <errno.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>
#include <poll.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <arpa/inet.h>

/* NMS includes */
#include <ctadef.h>
#include <adidef.h>
#include <vcedef.h>
#include <swidef.h>
#include <nccdef.h>
#include <isdnval.h>
#include <nccxadi.h>
#include <nccxisdn.h>
#include <imgtdef.h>
#include <imgtsvc.h>
/* NMS ISDN includes */
#include <isdndef.h>
#include <isdntype.h>
#include <isdnacu.h>
#include <isdnval.h>
#include <isdnparm.h>

#include <asterisk/version.h>
#if ASTERISK_VERSION_NUM >= 10400
#include <asterisk.h>
#endif
#include <asterisk/module.h>
#include <asterisk/lock.h>
#include <asterisk/channel.h>
#include <asterisk/frame.h>
#include <asterisk/config.h>
#include <asterisk/logger.h>
#include <asterisk/module.h>
#include <asterisk/pbx.h>
#include <asterisk/options.h>
#include <asterisk/lock.h>
#include <asterisk/sched.h>
#include <asterisk/io.h>
#include <asterisk/rtp.h>
#include <asterisk/acl.h>
#include <asterisk/callerid.h>
#include <asterisk/file.h>
#include <asterisk/cli.h>
#include <asterisk/indications.h>
#include <asterisk/causes.h>

/* debug information output helpers */
#define msg(level, s)      ast_verbose(level "chan_nms:%d %s: %s\n",    __LINE__, __FUNCTION__, s);
#define msg2(level, s, p)  ast_verbose(level "chan_nms:%d %s: %s %s\n", __LINE__, __FUNCTION__, s, p);
#define msg2p(level, s, p) ast_verbose(level "chan_nms:%d %s: " s "\n", __LINE__, __FUNCTION__, p);
#define msg3p(level, s, p, p2) ast_verbose(level "%s:%d %s: " s "\n", __FILE__, __LINE__, __FUNCTION__, p, p2);
#define warn(s)           do { if (option_verbose > 0) { msg(  VERBOSE_PREFIX_1, s); } } while (0)
#define warn2(s, p)       do { if (option_verbose > 0) { msg2( VERBOSE_PREFIX_1, s, p); } } while (0)
#define warn2p(s, p)      do { if (option_verbose > 0) { msg2p(VERBOSE_PREFIX_1, s, p); } } while (0)
#define warn3p(s, p, p2)  do { if (option_verbose > 0) { msg3p(VERBOSE_PREFIX_1, s, p, p2); } } while (0)
#define info(s)           do { if (option_verbose > 1) { msg(  VERBOSE_PREFIX_2, s); } } while (0)
#define info2(s, p)       do { if (option_verbose > 1) { msg2( VERBOSE_PREFIX_2, s, p); } } while (0)
#define info2p(s, p)      do { if (option_verbose > 1) { msg2p(VERBOSE_PREFIX_2, s, p); } } while (0)
#define info3p(s, p, p2)  do { if (option_verbose > 1) { msg3p(VERBOSE_PREFIX_2, s, p, p2); } } while (0)
#define debug(s)          do { if (option_verbose > 2) { msg(  VERBOSE_PREFIX_3, s); } } while (0)
#define debug2(s, p)      do { if (option_verbose > 2) { msg2( VERBOSE_PREFIX_3, s, p); } } while (0)
#define debug2p(s, p)     do { if (option_verbose > 2) { msg2p(VERBOSE_PREFIX_3, s, p); } } while (0)
#define debug3p(s, p, p2) do { if (option_verbose > 2) { msg3p(VERBOSE_PREFIX_3, s, p, p2); } } while (0)

/* configuration options value mappings */
typedef struct {
    char *name;
    int   value;
} nms_option_values;

/* see nms/include/isdnparm.h */
static nms_option_values option_operators [] = {
    { "FT_VN6",      ISDN_OPERATOR_FT_VN6 },
    { "ATT_4ESS",    ISDN_OPERATOR_ATT_4ESS },
    { "NT_DMS",      ISDN_OPERATOR_NT_DMS },
    { "NT_DMS250",   ISDN_OPERATOR_NT_DMS250 },
    { "NTT",         ISDN_OPERATOR_NTT },
    { "ETSI",        ISDN_OPERATOR_ETSI },
    { "AUSTEL_1",    ISDN_OPERATOR_AUSTEL_1 },
    { "ECMA_QSIG",   ISDN_OPERATOR_ECMA_QSIG },
    { "HK_TEL",      ISDN_OPERATOR_HK_TEL },
    { "NI2",         ISDN_OPERATOR_NI2 },
    { "ATT_5E10",    ISDN_OPERATOR_ATT_5E10 },
    { "ATT_5E9",     ISDN_OPERATOR_ATT_5E9 },
    { "KOREAN_OP",   ISDN_OPERATOR_KOREAN_OP },
    { "TAIWAN_OP",   ISDN_OPERATOR_TAIWAN_OP },
    { "DPNSS",       ISDN_OPERATOR_DPNSS },
    { "ANSI_T1_607", ISDN_OPERATOR_ANSI_T1_607 },
    { NULL, 0}
};

static nms_option_values option_countries [] = {
    { "AUS",       COUNTRY_AUS },
    { "BEL",       COUNTRY_BEL },
    { "EUR",       COUNTRY_EUR },
    { "FRA",       COUNTRY_FRA },
    { "GER",       COUNTRY_GER },
    { "HONG_KONG", COUNTRY_HONG_KONG },
    { "JPN",       COUNTRY_JPN },
    { "KOREA",     COUNTRY_KOREA },
    { "SINGAPORE", COUNTRY_SINGAPORE },
    { "SWE",       COUNTRY_SWE },
    { "CHINA",     COUNTRY_CHINA },
    { "TAIWAN",    COUNTRY_TAIWAN },
    { "GBR",       COUNTRY_GBR },
    { "USA",       COUNTRY_USA },
    { NULL, 0}
};

static nms_option_values option_partner_equipment_modes [] = {
    { "TE", EQUIPMENT_TE },
    { "NT", EQUIPMENT_NT },
    { NULL, 0}
};

#define E1 30
#define T1 23

static nms_option_values option_timeslots [] = {
    { "E1", E1 },
    { "T1", T1 },
    { NULL, 0}
};

static nms_option_values option_messaging [] = {
    { "PROGRESS", 0 },
    { "ALERTING", 1 },
    { NULL, 0}
};

static nms_option_values option_vocoders [] = {
    { "none", 0 },
    { "g723", AST_FORMAT_G723_1 },
    { "g729", AST_FORMAT_G729A },
    { "all",  AST_FORMAT_G723_1 | AST_FORMAT_G729A },
    { NULL, 0}
};

#if ASTERISK_VERSION_NUM >= 10400
#define AST_MODULE "chan_nms"
#endif
#define CHAN_NMS_CONFIG_FILENAME "nms.conf"
#define NMS_MODULE_DESC "NMS Open Access channel"

static const char module_desc[] = NMS_MODULE_DESC;
static const char dialplan_type[] = "NMS";
static const char channel_driver_desc[] = "NMS Open Access channel driver";

static int usecnt = 0;
AST_MUTEX_DEFINE_STATIC(usecnt_lock);

/* Protect the interface list (of nms_pvt's) */
AST_MUTEX_DEFINE_STATIC(nmslock);

static volatile int channel_seq_for_ast_name = 0;
static volatile int monitors_stop = 0;

#define ADI_MAX_FRAME_SIZE 320 /* 20ms 8kHz 16-bit signed linear */
#define CHANNEL_RECORD_BUFFER_FRAME_COUNT 4 /* must be power of two; 80ms is surely enough to send frame out */

struct nms_ast_frame {
    struct ast_frame frame;
    char friendly_offset[AST_FRIENDLY_OFFSET];
    char data[ADI_MAX_FRAME_SIZE];
} __attribute__ ((__packed__)); /* XXX packed for cheap arithmetics in get_frame_ptr() */

struct nms_pvt {
    ast_mutex_t lock;
    ast_cond_t cond;
    struct ast_channel *owner;

    CTAHD cta_hd;
    NCC_CALLHD call_hd;
    int userid;
    int board;
    int slot;
    int connected;
    int playing;
    int next_frame_i;
    int record_format;
    int play_format;
    struct nms_ast_frame frames[CHANNEL_RECORD_BUFFER_FRAME_COUNT];
};

static struct nms_pvt **nms_channels = NULL;

static struct ast_channel *nms_request(const char *type, int format, void *data, int *cause);
static int nms_digit(struct ast_channel *ast, char digit);
static int nms_call(struct ast_channel *ast, char *dest, int timeout);
static int nms_hangup(struct ast_channel *ast);
#if ASTERISK_VERSION_NUM >= 10400
static int nms_indicate(struct ast_channel *ast, int condition, const void *data, size_t datalen);
#else
static int nms_indicate(struct ast_channel *ast, int condition);
#endif
static int nms_answer(struct ast_channel *ast);
/* static struct ast_frame *nms_read(struct ast_channel *ast); */
static int nms_write(struct ast_channel *ast, struct ast_frame *frame);
/* static struct ast_frame *nms_exception(struct ast_channel *ast); */
static int nms_send_text(struct ast_channel *ast, const char *text);
static int nms_fixup(struct ast_channel *old, struct ast_channel *new);

static int nms_init(void);
static void nms_stop(void);
static void wait_event_q_timeout(CTAQUEUEHD ctaqueue_hd, CTA_EVENT *eventp, int timeout);
//static void wait_event_q(CTAQUEUEHD ctaqueue_hd, CTA_EVENT *eventp);
/* XXX
static int nms_digit_begin(struct ast_channel *chan, char digit)
{
        debug2p("digit '%c'", digit);
        nms_digit(chan, digit);
        return 0;
}

static int nms_digit_end(struct ast_channel *ast, char digit, unsigned int duration)
{
        debug3p("digit '%c'; duration %u", digit, duration);
        return 0;
}
*/
static struct ast_channel_tech nms_tech; /* forward declaration */

#if ASTERISK_VERSION_NUM >= 10601
#define AST_FRAME_DATA data.ptr
#else
#define AST_FRAME_DATA data
#endif

static pthread_t trunks_monitor_thread = AST_PTHREADT_NULL;
static pthread_t timeslots_monitor_thread = AST_PTHREADT_NULL;

#define TRUNKS_USERID_OFFSET 1000
#define MONITORS_EVENT_WAIT_TIMEOUT 1000 /* ms */

/* configuration */
static int operator = ISDN_OPERATOR_ETSI; /* defaults to Europe */
static int country = COUNTRY_EUR;

static int board_no = 0; /* XXX implement multiple boards support */
static int num_trunks = -1;
static volatile int trunks_in_service;
static int num_timeslots_per_trunk = -1;
static int num_timeslots = -1;
static const int mvip_stream = 64; /* XXX cg6565 mvip stream */
static int num_services;
static const int start_trunks = 1; /* XXX it might be possible to start trunks by external program */
static const int send_maint_messages = 1; /* XXX and send ISDN SERVICE maintenance message */
static int *partner_equipment_modes; /* these are _partner_ equipment modes (NT/TE) */
static int accept_alerting; /* send ALERTING or PROGRESS */
static int native_formats;

static char *dialplan_context_default = "default";
static char *dialplan_context;

CTA_INIT_PARMS initparms = {
    .size = sizeof(CTA_INIT_PARMS),
    .parmflags  = CTA_PARM_MGMT_LOCAL,
    .traceflags = CTA_TRACE_ENABLE,
    .daemonflags = 0,
    .ctaflags = CTA_MODE_LIBRARY,
    .filename = NULL,
    .ctacompatlevel = 0, // CTA_COMPATLEVEL,
    .reserved = 0
};
CTAQUEUEHD  trunks_q = NULL_CTAQUEUEHD;
CTAQUEUEHD  timeslots_q = NULL_CTAQUEUEHD;
CTAHD      *trunks_hd;
CTAHD      *timeslots_hd;
CTA_SERVICE_NAME servicelist[] = {
    { "ADI",  "ADIMGR" },
    { "ISDN", "ADIMGR" },
    { "NCC",  "ADIMGR" },
    { "SWI",  "SWIMGR" },
    { "IMGT", "ADIMGR" }
};
CTA_SERVICE_DESC services[] = {
    { .name = {"ADI",  "ADIMGR" }, .svcaddr = { {0} }, .svcargs = { {0} }, .mvipaddr = { 0 } },
    { .name = {"ISDN", "ADIMGR" }, .svcaddr = { {0} }, .svcargs = { {0} }, .mvipaddr = { 0 } },
    { .name = {"NCC",  "ADIMGR" }, .svcaddr = { {0} }, .svcargs = { {0} }, .mvipaddr = { 0 } },
    { .name = {"SWI",  "SWIMGR" }, .svcaddr = { {0} }, .svcargs = { {0} }, .mvipaddr = { 0 } },
    { .name = {"IMGT", "ADIMGR" }, .svcaddr = { {0} }, .svcargs = { {0} }, .mvipaddr = { 0 } }
};

/* */
static int option_value(nms_option_values *category, const char *name) {
    int i, value;
    char *end_ptr;

    if (ast_strlen_zero(name))
        return -1;

    for (i = 0; category[i].name != NULL; ++i) {
        if (!strcmp(name, category[i].name))
            return category[i].value;
    }

    /* you can specify a number as value in case you really know what you're doing */
    value = (int)strtol(name, &end_ptr, 0);
    if (*end_ptr != '\0') /* the string cannot be fully consumed be strtol() */
        return -1;
    return value;
}

/* */
static char* option_name(nms_option_values *category, int value) {
    int i;
    static char value_str[30];

    for (i = 0; category[i].name != NULL; ++i) {
        /* if there are parameters with different names but same value, we return the first one, thats ok */
        if (value == category[i].value)
            return category[i].name;
    }

    snprintf(value_str, sizeof(value_str), "%d (unknown)", value); // XXX not thread-safe
    return value_str;
}

/* dest format is:
   callee
   or trunk/calee
   or trunk/timeslot/callee
*/
static int get_trunk_num_from_dest(char *dest)
{
    char *slash = index(dest, '/');
    if (!slash) return -1;
    return atoi(dest);
}

static int get_timeslot_num_from_dest(char *dest)
{
    char *slash = index(dest, '/');
    if (!slash) return -1;
    char *slash2 = index(slash+1, '/');
    if (!slash2) return -1;
    return atoi(slash+1);
}

static char* get_callee_num_from_dest(char *dest)
{
    char *slash = index(dest, '/');
    if (!slash) return dest;
    return slash+1;
}

#if ASTERISK_VERSION_NUM >= 10400
static struct ast_channel *nms_new(int state, char *cid_num, char *cid_name, char *exten, char *context, char *name)
#else
static struct ast_channel *nms_new()
#endif
{
    debug("entered, allocating channel");
#if ASTERISK_VERSION_NUM >= 10400
    struct ast_channel *chan = ast_channel_alloc(1, state, cid_num, cid_name,
        NULL /* acctcode */, exten, context, 0 /* amaflag */, name);
#else
    struct ast_channel *chan = ast_channel_alloc(1); /* 1 - need queue */
#endif
    if (!chan) {
        ast_log(LOG_WARNING, "Unable to allocate channel structure\n");
        return NULL;
    }
    debug("allocating pvt");
    struct nms_pvt *p = malloc(sizeof(struct nms_pvt));
    if (!p) {
        ast_log(LOG_WARNING, "Unable to allocate private channel structure\n");
        ast_channel_free(chan);
        return NULL;
    }
    memset(p, 0, sizeof(*p));
    ast_mutex_init(&p->lock);
    ast_cond_init(&p->cond, NULL);
    p->owner = chan;
    p->cta_hd = NULL_CTAHD;
    p->call_hd = NULL_NCC_CALLHD;
    p->userid = -1;
    chan->tech_pvt = p;
    debug("pvt initialized");
    chan->tech = &nms_tech;
    chan->nativeformats = native_formats;
#if ASTERISK_VERSION_NUM < 10400
    chan->type = dialplan_type;
#endif
    debug("exiting with channel structure initialized");
    return chan;
}

static struct ast_channel *nms_request(const char *type, int format, void *data, int *cause)
{
    debug2("entered, data is", (char*)data);

    int desired_trunk = get_trunk_num_from_dest(data);
    if (desired_trunk < -1 || desired_trunk >= num_trunks) {
        ast_log(LOG_WARNING, "Trunk number out of range\n");
        *cause = AST_CAUSE_UNALLOCATED;
        return NULL;
    }
    int desired_timeslot = get_timeslot_num_from_dest(data);
    if (desired_timeslot < -1 || desired_timeslot >= num_timeslots_per_trunk) {
        ast_log(LOG_WARNING, "Timeslot number out of range\n");
        *cause = AST_CAUSE_UNALLOCATED;
        return NULL;
    }

    debug("calling nms_new()");
#if ASTERISK_VERSION_NUM >= 10400
    struct ast_channel *chan = nms_new(AST_STATE_DOWN,
        "", "", /* cide num & name */
        get_callee_num_from_dest(data), "" /* context */,
        "NMS"); // XXX name
#else
    struct ast_channel *chan = nms_new();
#endif
    if (chan) {
        chan->readformat = chan->writeformat = format;
        if (format & native_formats)
            chan->rawreadformat = chan->rawwriteformat = format;
        else
            chan->rawreadformat = chan->rawwriteformat = AST_FORMAT_SLINEAR;
        int slot_from = 0;
        int slot_to = num_timeslots;
        if (desired_trunk != -1) {
            if (desired_timeslot != -1) {
                slot_from = slot_to = desired_trunk * num_timeslots_per_trunk + desired_timeslot;
            } else {
                slot_from =  desired_trunk      * num_timeslots_per_trunk;
                slot_to   = (desired_trunk + 1) * num_timeslots_per_trunk - 1;
            }
        }
        debug3p("timeslots from:to is %d:%d", slot_from, slot_to);
        ast_mutex_lock(&nmslock);
        int slot;
        struct nms_pvt *p = chan->tech_pvt;
        for (slot = slot_from; slot <= slot_to; ++slot) {
            if (nms_channels[slot] == NULL) {
                nms_channels[slot] = p; // XXX race with timeslots_monitor() incoming call
                break;
            }
        }
        ast_mutex_unlock(&nmslock);
        if (slot == slot_to + 1) {
            ast_log(LOG_WARNING, "No free timeslots\n");
            chan->tech_pvt = NULL;
            free(p);
            ast_channel_free(chan);
            *cause = AST_CAUSE_CONGESTION;
            return NULL;
        }
        debug2p("timeslot is %d", slot);
        p->board = 0;
        p->slot = slot;
        p->userid = slot;
        p->cta_hd = timeslots_hd[slot];
#if ASTERISK_VERSION_NUM < 10400
        snprintf(chan->name, sizeof(chan->name), "NMS/%d/%s-%04x", slot, (char*)data, channel_seq_for_ast_name++ & 0xffff);
        ast_setstate(chan, AST_STATE_DOWN);
#endif
        ast_mutex_lock(&usecnt_lock);
        usecnt++;
        ast_mutex_unlock(&usecnt_lock);
        ast_update_use_count();
    }
    debug("exiting");
    return chan;
}

static struct ast_frame *alloc_frame(struct nms_pvt *p)
{
    int i = p->next_frame_i;
    struct ast_frame *frame = &p->frames[i].frame;
    memset(frame, 0, sizeof(*frame));
    frame->AST_FRAME_DATA = p->frames[i].data;
    frame->offset = AST_FRIENDLY_OFFSET;
    p->next_frame_i = (i + 1) & (CHANNEL_RECORD_BUFFER_FRAME_COUNT - 1);
    return frame;
}

static struct ast_frame *get_frame_ptr(void *data)
{
    return (struct ast_frame*)(data - AST_FRIENDLY_OFFSET - sizeof(struct ast_frame));
}

static int ast_format2nms(int format)
{
    switch (format) {
        default:
        case AST_FORMAT_SLINEAR: return ADI_ENCODE_PCM8M16;
        case AST_FORMAT_ALAW:    return ADI_ENCODE_ALAW;
        case AST_FORMAT_ULAW:    return ADI_ENCODE_MULAW;
        case AST_FORMAT_ADPCM:   return ADI_ENCODE_IMA_32;
        case AST_FORMAT_G726:    return ADI_ENCODE_G726_32;
        case AST_FORMAT_G729A:   return ADI_ENCODE_G729A;
        case AST_FORMAT_G723_1:  return ADI_ENCODE_G723_6; /* _5 for 5.3kbps */
        case AST_FORMAT_GSM:     return ADI_ENCODE_GSM;
    }
}

static int ast_format2datalen(int format)
{
    switch (format) {
        default:
        case AST_FORMAT_SLINEAR: return 320; /* 20ms */
        case AST_FORMAT_ALAW:    return 160;
        case AST_FORMAT_ULAW:    return 160;
        case AST_FORMAT_ADPCM:   return 92;
        case AST_FORMAT_G726:    return 80;
        case AST_FORMAT_G729A:   return 20;
        case AST_FORMAT_G723_1:  return 24; /* 30ms */
        case AST_FORMAT_GSM:     return 130; /* 80ms */
    }
}

static int ast_format2samples(int format)
{
    switch (format) {
        default:
        case AST_FORMAT_SLINEAR:
        case AST_FORMAT_ALAW:
        case AST_FORMAT_ULAW:
        case AST_FORMAT_ADPCM:
        case AST_FORMAT_G726:
        case AST_FORMAT_G729A:   return 160;
        case AST_FORMAT_G723_1:  return 240;
        case AST_FORMAT_GSM:     return 640;
    }
}

static struct ast_frame* setup_frame(struct nms_pvt *p)
{
    struct ast_channel *chan = p->owner;
    if (!chan) {
        warn("userid %d: called with chan == NULL");
        return NULL;
    }
    p->record_format = (chan->rawreadformat != 0) ? chan->rawreadformat : AST_FORMAT_SLINEAR;
    struct ast_frame *fr = alloc_frame(p);
    fr->frametype = AST_FRAME_VOICE;
    fr->subclass = p->record_format;
    fr->samples = ast_format2samples(p->record_format);
    fr->datalen = ast_format2datalen(p->record_format);
    return fr;
}

static void *timeslots_monitor(void *data)
{
    const CTAQUEUEHD queue = *(CTAQUEUEHD*)data;
    CTA_EVENT event = { 0 };
    int play_event = 0, record_event = 0;
    int play_underrun = 0, record_underrun = 0;
    while (!monitors_stop || trunks_in_service) {
        wait_event_q_timeout(queue, &event, MONITORS_EVENT_WAIT_TIMEOUT);
        if (event.id == CTAEVN_WAIT_TIMEOUT) continue;

        int userid = event.userid;
        if (userid >= num_timeslots) {
            ast_log(LOG_WARNING, "Event for unknown userid: %d\n", userid);
            continue;
        }
        //int board = event.userid >> 16;
        int board = 0; // XXX
        int slot  = userid & 0xFFFF;
        char name[16];
        ast_mutex_lock(&nmslock);
        struct nms_pvt *p = nms_channels[userid];
        ast_mutex_unlock(&nmslock);
        struct ast_channel *chan = NULL;
        if (p) chan = p->owner;
        struct ast_frame *fr = NULL;

        if (event.id != ADIEVN_RECORD_BUFFER_FULL && event.id != ADIEVN_PLAY_BUFFER_REQ)
            snprintf(name, sizeof(name), "NMS/%d", slot);

        switch (event.id) {
            case NCCEVN_SEIZURE_DETECTED:
                debug2("seizure detected on", name);
                break;

            case NCCEVN_INCOMING_CALL:
                {
                debug2("incoming call detected on", name);

                if (chan != NULL) {
                     ast_log(LOG_WARNING, "Currently only one call is supported at one timeslot, already busy as %s\n", chan->name); // XXX
                     break;
                }
                NCC_CALLHD      call_hd = NULL_NCC_CALLHD;
                NCC_CALL_STATUS nccstatus;

                call_hd = event.objHd;
                nccGetCallStatus(call_hd, &nccstatus, sizeof(nccstatus));

                debug("calling nms_new()");
#if ASTERISK_VERSION_NUM < 10400
                chan = nms_new();
                if (!chan)
                    break;
                p = chan->tech_pvt;
                p->board = board;
                p->slot = slot;
                p->userid = userid;
                p->cta_hd = timeslots_hd[userid];
                p->call_hd = call_hd;

                ast_setstate(chan, AST_STATE_RING);
                chan->rings = 0;
                strncpy(chan->context, dialplan_context, sizeof(chan->context)-1);
                if (!ast_strlen_zero(nccstatus.callingaddr)) {
                    chan->cid.cid_num = strdup(nccstatus.callingaddr);
                    chan->cid.cid_ani = strdup(nccstatus.callingaddr);
                }
                if (!ast_strlen_zero(nccstatus.callingname))
                    chan->cid.cid_name = strdup(nccstatus.callingname);
                char *callee_num;
                if (!ast_strlen_zero(nccstatus.calledaddr)) {
                    strncpy(chan->exten, nccstatus.calledaddr, sizeof(chan->exten) - 1);
                    callee_num = nccstatus.calledaddr;
                } else
                    callee_num = "s";

                snprintf(chan->name, sizeof(chan->name), "%s/%s-%04x", name, callee_num, channel_seq_for_ast_name++ & 0xffff);
#else
                debug2("called addr is", nccstatus.calledaddr);
                chan = nms_new(AST_STATE_RING,
                    nccstatus.callingaddr, nccstatus.callingname,
                    nccstatus.calledaddr, dialplan_context,
                    name); // XXX name
                if (!chan)
                    break;
                p = chan->tech_pvt;
                p->board = board;
                p->slot = slot;
                p->userid = userid;
                p->cta_hd = timeslots_hd[userid];
                p->call_hd = call_hd;
#endif
                // XXX start with slin
                chan->readformat = chan->writeformat = chan->rawreadformat = chan->rawwriteformat = AST_FORMAT_SLINEAR;
                ast_mutex_lock(&usecnt_lock);
                usecnt++;
                ast_mutex_unlock(&usecnt_lock);
                ast_update_use_count();

                nms_channels[userid] = p;
                //ast_mutex_unlock(&nmslock);

                info("starting pbx");
                if (ast_pbx_start(chan)) {
                    info("ast_pbx_start() failed");
                    ast_log(LOG_WARNING, "Unable to start PBX on %s\n", name);
                    ast_hangup(chan);
                    break;
                }
                }
                break;

            case NCCEVN_PLACING_CALL:
                if (!chan) break;
                info("setting channel state to DIALING");
                ast_setstate(chan, AST_STATE_DIALING);
                break;

            case NCCEVN_CALL_PROCEEDING:
                if (!chan) break;
                info("call proceeded successfully");
                break;

            case NCCEVN_PROTOCOL_EVENT:
                if (!chan) break;
                if (event.value == ISDN_PROGRESS)
                    info("call progress indicated");
                else
                    info("unknown call control protocol event");
                break;

            case NCCEVN_REMOTE_ALERTING:
                if (!chan) break;
                info("setting channel state to RINGING");
                ast_setstate(chan, AST_STATE_RINGING);
                break;

            case NCCEVN_REMOTE_ANSWERED:
                break;

            case NCCEVN_ANSWERING_CALL:
            case NCCEVN_ACCEPTING_CALL:
            case NCCEVN_REJECTING_CALL:
                break;

            case NCCEVN_EXTENDED_CALL_STATUS_UPDATE:
                if (!p) break;
                {
                NCC_ISDN_EXT_CALL_STATUS status;
                nccGetExtendedCallStatus(p->call_hd, &status, sizeof(status));
                if (event.value & NCC_X_STATUS_INFO_PROGRESSDESCR)
                    debug2p("Q.931 progress description: %d", (int)status.progressdescr);
                if (event.value & NCC_X_STATUS_INFO_CAUSE)
                    debug3p("Q.931 release cause: %d: %s", (int)status.releasecause, ast_cause2str(status.releasecause));
                }
                break;

            case NCCEVN_CALL_CONNECTED:
                /*
                {
                NCC_CALL_STATUS nccstatus;
                nccGetCallStatus(p->call_hd, &nccstatus, sizeof(nccstatus));
                debug3p("called addr (try2) is: %s; calling addr is: %s", nccstatus.calledaddr, nccstatus.callingaddr);
                }
                {
                NCC_ISDN_EXT_CALL_STATUS status;
                nccGetExtendedCallStatus(p->call_hd, &status, sizeof(status));
                debug2p("called addr (try3) is: %s; %s", status.connectedaddr, status.calledsubaddr);
                }
                */
                if (!chan) break;
                info("setting channel state to UP");
                p->connected = 1;
                ast_setstate(chan, AST_STATE_UP);
                ast_cond_broadcast(&p->cond); /* wakeup nms_answer() */
                /* fall through */
            case ADIEVN_RECORD_DONE:
                if (p->connected) {
                    if (!chan) break;
                    ADI_RECORD_PARMS parms;
                    ctaGetParms(p->cta_hd, ADI_RECORD_PARMID, &parms, sizeof(parms));
                    parms.novoicetime = 0;
                    parms.silencetime = 0;
                    parms.DTMFabort = 0;
                    parms.beepfreq = 0; /* disable beep, for record function to not seize the port, otherwise PlayAsync() will get CTAERR_OUTPUT_ACTIVE */
                    fr = setup_frame(p);
                    debug3p("calling adiRecordAsync() with format: %d; datalen: %d", p->record_format, fr->datalen);
                    adiRecordAsync(p->cta_hd, ast_format2nms(p->record_format), 10*100*1000*1000, /* 10*... - mS - forever */
                        fr->data, fr->datalen, &parms);
                }
                break;

            case ADIEVN_RECORD_BUFFER_FULL:
                if (!chan) break;
                if (!(record_event++%1000))
                    debug2p("record buffer requested %d", record_event);
                if (event.value & ADI_RECORD_UNDERRUN) {
                    if (record_underrun++ < 100 || !(record_underrun%100))
                        debug3p("userid %d; record underrun occured: %d", p->userid, record_underrun);
                    if (record_underrun == 100)
                        debug("rate limiting record underrun messages");
                }
                ast_queue_frame(chan, get_frame_ptr(event.buffer)); /* hope it will release the frame soon */
                /* fall through */
            case ADIEVN_RECORD_STARTED:
                if (event.value & ADI_RECORD_BUFFER_REQ) {
                    if (!chan) break;
                    if (p->record_format != chan->rawreadformat) {
                        debug3p("record format changed: %d -> %d; calling adiStopRecording()", p->play_format, chan->rawreadformat);
                        adiStopRecording(p->cta_hd);
                        break;
                    }
                    fr = setup_frame(p);
                    adiSubmitRecordBuffer(p->cta_hd, fr->data, fr->datalen);
                }
                else
                    debug("record buffer not requested");
                break;

            case ADIEVN_PLAY_BUFFER_REQ:
                if (!(play_event++%1000))
                    debug2p("play buffer requested %d", play_event);
                if (event.value & ADI_PLAY_UNDERRUN) {
                    if (play_underrun++ < 100 || !(play_underrun%100))
                        debug3p("userid %d; play underrun occured: %d", p->userid, play_underrun);
                    if (play_underrun == 100)
                        debug("rate limiting play underrun messages");
                }
                break;

            case ADIEVN_PLAY_DONE:
                if (!p) break;
                ast_mutex_lock(&p->lock);
                p->playing = 0;
                ast_cond_broadcast(&p->cond); /* wakeup nms_write() */
                ast_mutex_unlock(&p->lock);
                break;

            case ADIEVN_DIGIT_BEGIN:
                debug2p("digit received: %c", (int)event.value);
                if (!p) break;
                fr = alloc_frame(p);
                memset(fr, 0, sizeof(*fr));
                fr->frametype = AST_FRAME_DTMF;
                fr->subclass = event.value;
                ast_queue_frame(chan, fr);
                break;

            case ADIEVN_DIGIT_END:
                break;

            case NCCEVN_CALL_DISCONNECTED:
                if (!p) break;
                {
                p->connected = 0;
                NCC_ISDN_EXT_CALL_STATUS status;
                nccGetExtendedCallStatus(p->call_hd, &status, sizeof(status));
                int cause = status.releasecause;
                debug3p("Q.931 release cause: %d: %s", cause, ast_cause2str(cause));
                ast_mutex_lock(&p->lock);
                chan = p->owner;
                if (chan) {
                    p->owner = NULL;
                    chan->tech_pvt = NULL;
                }
                ast_mutex_unlock(&p->lock);
                debug2p("userid: %d; calling nccReleaseCall", p->userid);
                nccReleaseCall(p->call_hd, NULL);
                if (chan) {
                    debug2p("userid: %d; calling ast_queue_hangup()", p->userid);
                    if (cause == 0) { /* unknown */
                        switch (event.value) {
                            case NCC_DIS_REORDER: cause = AST_CAUSE_CONGESTION; break;
                            case NCC_DIS_BUSY: cause = AST_CAUSE_USER_BUSY; break;
                        }
                        if (cause != 0)
                            debug3p("hangup cause is: %d: %s", cause, ast_cause2str(cause));
                    }
                    // XXX race-condition with nms_answer() returning from wait for NCCEVN_CALL_CONNECTED
                    // with timeout causing channel to hangup by asterisk
                    chan->hangupcause = cause;
                    ast_queue_hangup(chan);
                }
                }
                break;

            case NCCEVN_CALL_RELEASED:
                if (!p) break;
                ast_mutex_lock(&nmslock);
                nms_channels[p->userid] = NULL;
                ast_mutex_unlock(&nmslock);
                free(p);
                break;

            case NCCEVN_CAPABILITY_UPDATE:
            case ADIEVN_BOARD_ERROR:
                break;

            default:
                ast_log(LOG_WARNING, "Unknown event\n");
                break;
        }
    }
    debug("exiting");
    return NULL;
}

static void *trunks_monitor(void *data)
{
    const CTAQUEUEHD queue = *(CTAQUEUEHD*)data;
    CTA_EVENT event = { 0 };
    while (!monitors_stop || trunks_in_service) {
        wait_event_q_timeout(queue, &event, MONITORS_EVENT_WAIT_TIMEOUT);
        if (event.id == CTAEVN_WAIT_TIMEOUT) continue;

        int userid = event.userid;
        if (userid < TRUNKS_USERID_OFFSET || userid >= TRUNKS_USERID_OFFSET + num_trunks) {
            ast_log(LOG_WARNING, "Event for unknown userid: %d\n", userid);
            continue;
        }
        int trunk = userid - TRUNKS_USERID_OFFSET;
        switch (event.id) {
            case IMGTEVN_RCV_MESSAGE:
                break;

            case ISDNEVN_STOP_PROTOCOL: //NCCEVN_STOP_PROTOCOL_DONE:
                info2p("trunk shutdown: %d", trunk);
                --trunks_in_service;
                break;

            default:
                ast_log(LOG_WARNING, "Unknown event\n");
                break;
        }
    }
    debug("exiting");
    return NULL;
}

static int nms_hangup(struct ast_channel *chan)
{
    debug("entered");

    //int state = chan->_state != AST_STATE_UP
    info3p("hangup %s: %s", chan->name, ast_cause2str(chan->hangupcause));
    ast_mutex_lock(&nmslock);
    struct nms_pvt *p = chan->tech_pvt;
    if (p) {
        ast_mutex_lock(&p->lock);
        ast_mutex_unlock(&nmslock);
        p->owner = NULL;
        chan->tech_pvt = NULL;
        ast_mutex_unlock(&p->lock);
        debug("calling nccDisconnectCall");
        DISCONNECTCALL_EXT dp = { .size = sizeof(dp), .ie_list = {0}, .cause = chan->hangupcause };
        nccDisconnectCall(p->call_hd, &dp);
    } else
        ast_mutex_unlock(&nmslock);

    ast_setstate(chan, AST_STATE_DOWN);

    ast_mutex_lock(&usecnt_lock);
    --usecnt;
    ast_mutex_unlock(&usecnt_lock);
    ast_update_use_count();

    debug("exiting");
    return 0;
}

static int nms_call(struct ast_channel *chan, char *dest, int timeout)
{
    debug("entered");
    info2("calling", dest);
    struct nms_pvt *p = chan->tech_pvt;
    char *callee_num = get_callee_num_from_dest(dest);
    if (nccPlaceCall(p->cta_hd, callee_num, chan->cid.cid_num, NULL, NULL, &p->call_hd) != SUCCESS) {
        debug("exiting with failure");
        return -1;
    }
    debug("exiting");
    return 0;
}

static int nms_digit(struct ast_channel *chan, char digit)
{
    char digits[] = { digit, 0 };
    debug2("entered, sending", digits);
    /* sending DTMF natively requires us to stop PlayAsync, what a mess!
    struct nms_pvt *p = chan->tech_pvt;
    if (!p) return -1;
    adiStartDTMF(p->cta_hd, digits, NULL);
    */
    /* copied from chan_misdn, thanks! */
    static const char* dtmf_tones[] = {
        "!941+1336/100,!0/100", /* 0 */
        "!697+1209/100,!0/100", /* 1 */
        "!697+1336/100,!0/100", /* 2 */
        "!697+1477/100,!0/100", /* 3 */
        "!770+1209/100,!0/100", /* 4 */
        "!770+1336/100,!0/100", /* 5 */
        "!770+1477/100,!0/100", /* 6 */
        "!852+1209/100,!0/100", /* 7 */
        "!852+1336/100,!0/100", /* 8 */
        "!852+1477/100,!0/100", /* 9 */
        "!697+1633/100,!0/100", /* A */
        "!770+1633/100,!0/100", /* B */
        "!852+1633/100,!0/100", /* C */
        "!941+1633/100,!0/100", /* D */
        "!941+1209/100,!0/100", /* * */
        "!941+1477/100,!0/100"  /* # */
    };
    if (digit >= '0' && digit <='9')
        ast_playtones_start(chan, 0, dtmf_tones[digit-'0'], 0);
    else if (digit >= 'A' && digit <= 'D')
        ast_playtones_start(chan, 0, dtmf_tones[digit-'A'+10], 0);
    else if (digit == '*')
        ast_playtones_start(chan, 0, dtmf_tones[14], 0);
    else if (digit == '#')
        ast_playtones_start(chan, 0, dtmf_tones[15], 0);
    else
        ast_verbose(VERBOSE_PREFIX_3 "Unable to handle DTMF tone '%c' for '%s'\n", digit, chan->name);
    debug("exiting");
    return 0;
}

static int nms_answer(struct ast_channel *chan)
{
    debug("entered, calling nccAnswerCall()");
    struct nms_pvt *p = chan->tech_pvt;
    if (nccAnswerCall(p->call_hd, 0, NULL) != SUCCESS) {
        debug("exiting with failure");
        return -1;
    }
    ast_mutex_lock(&p->lock);
    while (chan->_state != AST_STATE_UP) {
        struct timeval now = ast_tvnow();
        struct timespec wait = { .tv_sec = now.tv_sec + 3, .tv_nsec = now.tv_usec*1000 }; // XXX timeout interval?
        int ret = ast_cond_timedwait(&p->cond, &p->lock, &wait);
        if (ret) {
            ast_mutex_unlock(&p->lock);
            debug2("exiting with failure waiting for condition", strerror(ret));
            return -1;
        }
    }
    ast_mutex_unlock(&p->lock);
    debug("exiting");
    return 0;
}

static nms_option_values ast_control_reasons [] = {
    { "AST_CONTROL_HANGUP - Other end has hungup",           AST_CONTROL_HANGUP },
    { "AST_CONTROL_RING - Local ring",                       AST_CONTROL_RING },
    { "AST_CONTROL_RINGING - Remote end is ringing",         AST_CONTROL_RINGING },
    { "AST_CONTROL_ANSWER - Remote end has answered",        AST_CONTROL_ANSWER },
    { "AST_CONTROL_BUSY - Remote end is busy",               AST_CONTROL_BUSY },
    { "AST_CONTROL_TAKEOFFHOOK - Make it go off hook",       AST_CONTROL_TAKEOFFHOOK },
    { "AST_CONTROL_OFFHOOK - Line is off hook",              AST_CONTROL_OFFHOOK },
    { "AST_CONTROL_CONGESTION - Congestion (circuits busy)", AST_CONTROL_CONGESTION },
    { "AST_CONTROL_FLASH - Flash hook",                      AST_CONTROL_FLASH },
    { "AST_CONTROL_WINK - Wink",                             AST_CONTROL_WINK },
    { "AST_CONTROL_OPTION - Set a low-level option",         AST_CONTROL_OPTION },
    { "AST_CONTROL_RADIO_KEY - Key Radio",                   AST_CONTROL_RADIO_KEY },
    { "AST_CONTROL_RADIO_UNKEY - Un-Key Radio",              AST_CONTROL_RADIO_UNKEY },
    { "AST_CONTROL_PROGRESS - Progress",                     AST_CONTROL_PROGRESS },
    { "AST_CONTROL_PROCEEDING - Call proceeding",            AST_CONTROL_PROCEEDING },
    { "AST_CONTROL_HOLD - Call is placed on hold",           AST_CONTROL_HOLD },
    { "AST_CONTROL_UNHOLD - Call is left from hold",         AST_CONTROL_UNHOLD },
    { "AST_CONTROL_VIDUPDATE - Video frame update",          AST_CONTROL_VIDUPDATE },
    { "AST_CONTROL_SRCUPDATE - Source of media has changed", AST_CONTROL_SRCUPDATE },
    { NULL, 0}
};

#if ASTERISK_VERSION_NUM >= 10400
static int nms_indicate(struct ast_channel *chan, int condition, const void *data, size_t datalen)
#else
static int nms_indicate(struct ast_channel *chan, int condition)
#endif
{
    debug2("indicating", option_name(ast_control_reasons, condition) /* ast_channel_reason2str(condition) is crap */);
    struct nms_pvt *p = chan->tech_pvt;
    switch (condition) {
        case AST_CONTROL_RINGING:
            debug("accepting call with RING tone");
            nccAcceptCall(p->call_hd, NCC_ACCEPT_PLAY_RING, NULL);
            break;
        case AST_CONTROL_BUSY:
            debug("rejecting call with BUSY tone");
            {
            REJECTCALL_EXT rc = { .size = sizeof(rc), .ie_list = {0}, .cause = AST_CAUSE_USER_BUSY };
            nccRejectCall(p->call_hd, NCC_REJECT_PLAY_BUSY, &rc);
            }
            break;
        case AST_CONTROL_CONGESTION:
            debug("rejecting call with REORDER tone");
            {
            REJECTCALL_EXT rc = { .size = sizeof(rc), .ie_list = {0}, .cause = AST_CAUSE_NO_ROUTE_DESTINATION };
            nccRejectCall(p->call_hd, NCC_REJECT_PLAY_REORDER, &rc);
            }
            break;
        default:
            debug("nothing to do");
            break;
    }
    return 0;
}

static int nms_write(struct ast_channel *chan, struct ast_frame *frame)
{
    struct nms_pvt *p = chan->tech_pvt;
    if (!p->connected) return 0; // call from ast_prod(), anyway we have indicate()

    if (frame->frametype != AST_FRAME_VOICE || !(frame->subclass & native_formats)) {
        warn3p("unsupported frame type %d; subclass %d", frame->frametype, frame->subclass);
        return -1;
    }

    if (p->play_format != frame->subclass) {
        ast_mutex_lock(&p->lock);
        while (p->playing) {
            debug3p("play format changed: %d -> %d; calling adiStopPlaying()", p->play_format, frame->subclass);
            adiStopPlaying(p->cta_hd);
            struct timeval now = ast_tvnow();
            struct timespec wait = { .tv_sec = now.tv_sec + 1, .tv_nsec = now.tv_usec*1000};
            int ret = ast_cond_timedwait(&p->cond, &p->lock, &wait);
            if (ret) {
                ast_mutex_unlock(&p->lock);
                warn2("exiting with failure waiting for condition", strerror(ret));
                return -1;
            }
        }
        ast_mutex_unlock(&p->lock);
    }

    /* Cheat: do not copy frame but rely on card buffer instead, from NMS documentation:
       If bufsize is less than or equal to the board buffer size, you can reuse the buffer as soon as this function returns.
       Greatly simplifies memory management.
    */
    if (!p->playing) {
        debug3p("calling adiPlayAsync() with format: %d; length: %d", frame->subclass, frame->datalen);
        ADI_PLAY_PARMS parms;
        ctaGetParms(p->cta_hd, ADI_PLAY_PARMID, &parms, sizeof(parms));
        parms.DTMFabort = 0;
        if(adiPlayAsync(p->cta_hd, ast_format2nms(frame->subclass), frame->AST_FRAME_DATA, frame->datalen, 0, &parms) == SUCCESS) {
            p->playing = 1;
            p->play_format = frame->subclass;
        }
    } else {
        // XXX must smartly dial with ADIERR_TOO_MANY_BUFFERS error
        /*
        int i = 0, ret;
        while (i++ < 2 && (ret = adiSubmitPlayBuffer(p->cta_hd, fr->data, fr->datalen, 0)) == ADIERR_TOO_MANY_BUFFERS) {
            if (ast_safe_sleep(chan, 5) == -1)
                break;
        }
        */
        adiSubmitPlayBuffer(p->cta_hd, frame->AST_FRAME_DATA, frame->datalen, 0);
    }
    return 0;
}

static int nms_send_text(struct ast_channel *ast, const char *text)
{
    debug2("entered, sending", text);
    debug("exiting");
    return 0;
}

static int nms_fixup(struct ast_channel *old, struct ast_channel *new)
{
    debug("entered");
    struct nms_pvt *p = new->tech_pvt; // XXX should it be 'old'?
    p->owner = new;
    debug("exiting");
    return 0;
}

#if ASTERISK_VERSION_NUM < 10600
static int nms_show(int fd, int argc, char **argv)
{
    int i;
    struct ast_channel *chan;
    struct nms_pvt *p;

    if (argc != 3)
        return RESULT_SHOWUSAGE;
    ast_mutex_lock(&nmslock);
    if (!nms_channels)
        ast_cli(fd, "No NMS channels allocated\n");
    else {
        ast_cli(fd, "  id -- board/slot -- owner -- context -- exten\n");
        for(i = 0; i < num_timeslots; ++i) {
            p = nms_channels[i];
            if (p != NULL) {
                chan = p->owner;
                ast_cli(fd, "%4d -- %d/%d -- %s -- %s -- %s\n", i, p->board, p->slot, chan->name, chan->context, chan->exten);
            }
        }
    }
    ast_mutex_unlock(&nmslock);
    return RESULT_SUCCESS;
}

static char show_nms_usage[] =
"Usage: nms show channels\n"
"       Provides summary information on NMS channels.\n";

static struct ast_cli_entry cli_show_nms = {
    { "nms", "show", "channels", NULL }, nms_show,
    "Show status of NMS channels", show_nms_usage, NULL
};
#endif

static int parse_config(void)
{
#if ASTERISK_VERSION_NUM >= 10600
    struct ast_flags config_flags = { 0 };
    struct ast_config *cfg = ast_config_load(CHAN_NMS_CONFIG_FILENAME, config_flags);
#else
    struct ast_config *cfg = ast_config_load(CHAN_NMS_CONFIG_FILENAME);
#endif
    struct ast_variable *var;
    int ret = 0;
    char *partner = NULL;

    if (cfg == NULL) {
        warn2p("%s not found", CHAN_NMS_CONFIG_FILENAME);
        return -1;
    }

    dialplan_context = dialplan_context_default;
    for (var = ast_variable_browse(cfg, "cg6565"); var; var = var->next) {
        const char *name = var->name;
        /* the number of trunks */
        if (!strcasecmp(name, "trunks")) {
            num_trunks = atoi(var->value);
            if (num_trunks < 1 || num_trunks > 16) {
                warn2p("trunks must be [1:16]; trunks=%s is not valid", var->value);
                ret = -1;
            }
        /* E1 or T1 */
        } else if (!strcasecmp(name, "timeslots")) {
            num_timeslots_per_trunk = option_value(option_timeslots, var->value);
            if (num_timeslots_per_trunk != T1 && num_timeslots_per_trunk != E1) {
                warn2p("timeslots must be E1 or T1; timeslots=%s is not valid", var->value);
                ret = -1;
            }
        /* NT or TE mode on partner side */
        } else if (!strcasecmp(name, "partner_equipment_modes")) {
            if (!ast_strlen_zero(var->value))
                partner = ast_strdupa(var->value); /* should be processed after number of trunks is known */
        } else if (!strcasecmp(name, "country")) {
            country = option_value(option_countries, var->value);
        } else if (!strcasecmp(name, "operator")) {
            operator = option_value(option_operators, var->value);
        } else if (!strcasecmp(name, "accept")) {
            accept_alerting = option_value(option_messaging, var->value);
            if (accept_alerting == -1) {
                warn2p("accept must be PROGRESS or ALERTING; accept=%s is not valid", var->value);
                ret = -1;
            }
        } else if (!strcasecmp(name, "licensed_vocoders")) {
            int v = option_value(option_vocoders, var->value);
            if (v == -1) {
                warn2p("licensed_vocoders must be none, g723, g729, or all; licensed_vocoders=%s is not valid", var->value);
                ret = -1;
            }
            native_formats |= v;
        } else if (!strcasecmp(name, "context")) {
            dialplan_context = strdup(var->value);
        } else {
            warn2p("unrecognised option %s", name);
        }
    }
    ast_config_destroy(cfg);

    if (num_trunks == -1) {
        warn2p("trunks= must be specified in %s", CHAN_NMS_CONFIG_FILENAME);
        ret = -1;
    }
    if (num_timeslots_per_trunk == -1) {
        warn2p("timeslots= must be specified in %s", CHAN_NMS_CONFIG_FILENAME);
        ret = -1;
    }
    if (partner == NULL) {
        warn2p("partner_equipment_modes= must be specified in %s", CHAN_NMS_CONFIG_FILENAME);
        ret = -1;
    }
    if (ret == -1)
        return ret;

    num_timeslots = num_trunks * num_timeslots_per_trunk;
    partner_equipment_modes = malloc(num_trunks * sizeof(*partner_equipment_modes));

    int i = 0;
    char *mode;
    int value;
    while (partner != NULL && (mode = strsep(&partner, " \t")) != NULL) {
        if (mode[0] == '\0')
            continue;
        value = option_value(option_partner_equipment_modes, mode);
        if (value == -1) {
            warn2p("unrecognised mode in partner_equipment_modes: %s", mode);
            ret = -1;
            break;
        }
        if (i == num_trunks) {
            warn("number of trunks must match partner_equipment_modes");
            ret = -1;
            break;
        }
        partner_equipment_modes[i++] = value;
    }

    if (ret != 0)
        free(partner_equipment_modes);
    else {
        if (i != num_trunks) {
            warn("number of trunks must match partner_equipment_modes");
            free(partner_equipment_modes);
            ret = -1;
        }
    }

    return ret;
}

#if ASTERISK_VERSION_NUM < 10400
static int nms_unload_module();
static void nms_atexit(void)
{
    nms_unload_module();
}
#endif

static struct ast_channel_tech nms_tech = {
    .type = dialplan_type,
    .description = channel_driver_desc,
    .capabilities = AST_FORMAT_SLINEAR,
    .requester = nms_request,
#if ASTERISK_VERSION_NUM >= 10400
    .send_digit_begin = nms_digit, // _begin
    .send_digit_end = NULL, // nms_digit_end,
#else
    .send_digit = nms_digit,
#endif
    .call = nms_call,
    .hangup = nms_hangup,
    .answer = nms_answer,
    .read = NULL,
    .write = nms_write,
    .exception = NULL,
    .send_text = nms_send_text,
    .indicate = nms_indicate,
    .fixup = nms_fixup,
};

int load_module()
{
    int i;

    trunks_in_service = 0;
    accept_alerting = 0;
    native_formats = AST_FORMAT_SLINEAR | AST_FORMAT_ALAW | AST_FORMAT_ULAW | AST_FORMAT_ADPCM | AST_FORMAT_G726;
    /* AST_FORMAT_GSM is not enabled because ADI frame size is 80ms for an unknown reason which is too high
    iLBC is only for Fusion applications - can't record/play via ADI */
    if (parse_config() != 0)
        return -1;
    if (option_verbose > 1) {
        const int s = num_trunks*3; /* two-byte value: NT or TE + whitespace delimeter and '\0' at the end */
        char m[s];
        m[0] = '\0';
        for (i = 0; i < num_trunks; ++i) {
            /* will truncate output if unrecognised number is specified in parameter instead of NT/TE */
            strncat(m, option_name(option_partner_equipment_modes, partner_equipment_modes[i]), s - strlen(m) - 1);
            strncat(m, " ", s - strlen(m) - 1);
        }
        info2p("           trunks = %d", num_trunks);
        info2p("  total timeslots = %d", num_timeslots);
        info2p("    partner modes = %s", m);
        info2p("          country = %s", option_name(option_countries, country));
        info2p("         operator = %s", option_name(option_operators, operator));
        info2p("           accept = %s", option_name(option_messaging, accept_alerting));
        info2p("licensed_vocoders = %s", option_name(option_vocoders, native_formats & (AST_FORMAT_G723_1 | AST_FORMAT_G729A)));
    }

    ast_mutex_init(&nmslock);

    nms_channels = malloc(num_timeslots * sizeof(*nms_channels));
    for(i = 0; i < num_timeslots; ++i)
        nms_channels[i] = NULL;

    nms_tech.capabilities = native_formats;
    if (ast_channel_register(&nms_tech)) {
        ast_log(LOG_ERROR, "Unable to register channel class %s\n", dialplan_type);
        return -1;
    }
    if (nms_init()) {
        ast_log(LOG_ERROR, "Unable to start NMS software stack\n");
        return -1;
    }
    if (ast_pthread_create(&trunks_monitor_thread, NULL, trunks_monitor, &trunks_q) < 0) {
        ast_log(LOG_ERROR, "Unable to start NMS trunks monitor thread\n");
        return -1;
    }
    if (ast_pthread_create(&timeslots_monitor_thread, NULL, timeslots_monitor, &timeslots_q) < 0) {
        ast_log(LOG_ERROR, "Unable to start NMS timeslots monitor thread\n");
        return -1;
    }
#if ASTERISK_VERSION_NUM < 10600
    ast_cli_register(&cli_show_nms);
#endif
#if ASTERISK_VERSION_NUM < 10400
    ast_register_atexit(nms_atexit);
#endif
    return 0;
}

int reload()
{
    debug("entered");
    debug("exiting");
    return 0;
}

static int nms_unload_module()
{
    int i;

    debug("stopping");
    /* First, take us out of the channel loop */
    ast_channel_unregister(&nms_tech);
    if (ast_mutex_lock(&nmslock)) {
        ast_log(LOG_WARNING, "chan_nms: unable to lock nmslock\n");
        return -1;
    }
    /* Hangup all interfaces if they have an owner */
    for (i = 0; i < num_timeslots; ++i) {
        struct nms_pvt *p = nms_channels[i];
        if (p != NULL && p->owner != NULL)
            ast_softhangup(p->owner, AST_SOFTHANGUP_APPUNLOAD);
    }
    ast_mutex_unlock(&nmslock);
    nms_stop();
    monitors_stop = 1;
    void *ret;
    if (timeslots_monitor_thread != AST_PTHREADT_NULL)
        pthread_join(timeslots_monitor_thread, &ret);
    if (trunks_monitor_thread != AST_PTHREADT_NULL)
        pthread_join(trunks_monitor_thread, &ret);
#if ASTERISK_VERSION_NUM < 10600
    ast_cli_unregister(&cli_show_nms);
#endif
    free(nms_channels);
    nms_channels = NULL;
    if (dialplan_context != dialplan_context_default)
        free(dialplan_context);
    return 0;
}

#if ASTERISK_VERSION_NUM >= 10400
AST_MODULE_INFO(ASTERISK_GPL_KEY, AST_MODFLAG_DEFAULT, NMS_MODULE_DESC, .load = load_module, .unload = nms_unload_module, .buildopt_sum = "");
#else
int unload_module()
{
    return nms_unload_module();
}

int usecount()
{
    int res;
    ast_mutex_lock(&usecnt_lock);
    res = usecnt;
    ast_mutex_unlock(&usecnt_lock);
    return res;
}

char *key()
{
    return ASTERISK_GPL_KEY;
}

char *description()
{
    return (char*)module_desc;
}
#endif

/* NMS NA part */

DWORD NMSSTDCALL err_handler(CTAHD ctahd, DWORD errorcode, char *errortext, char *func)
{
    //if (errorcode == ADIERR_TOO_MANY_BUFFERS)
    //    return errorcode;
    if (ctahd == NULL_CTAHD)
        ast_log(LOG_ERROR, "CTA error in %s [ctahd=null]: %s (%#x)\n", func, errortext, (unsigned int)errorcode);
    else
        ast_log(LOG_ERROR, "CTA error in %s [ctahd=%#x]: %s (%#x)\n",  func, (unsigned int)ctahd, errortext, (unsigned int)errorcode);
    return errorcode;
}

/* imgtsvc.h */
static nms_option_values imgt_messages [] = {
    { "SERVICE_RQ - SERVICE message request",                IMGT_SERVICE_RQ },
    { "SERVICE_IN - SERVICE message indication",             IMGT_SERVICE_IN },
    { "SERVICE_CO - SERVICE message confirmation",           IMGT_SERVICE_CO },
    { "RESTART_IN - RESTART indication",                     IMGT_RESTART_IN },
    { "B_CHANNEL_STATUS_RQ - B-channel status request",      IMGT_B_CHANNEL_STATUS_RQ },
    { "B_CHANNEL_STATUS_CO - B-channel status indication",   IMGT_B_CHANNEL_STATUS_CO },
    { "SET_MASK_RQ - set mask request",                      IMGT_SET_MASK_RQ },
    { "SET_MASK_CO - mask confirmation",                     IMGT_SET_MASK_CO },
    { "D_CHANNEL_STATUS_RQ - D-channel status request",      IMGT_D_CHANNEL_STATUS_RQ },
    { "D_CHANNEL_STATUS_IN - D-channel status indication",   IMGT_D_CHANNEL_STATUS_IN },
    { "D_CHANNEL_STATUS_CO - D-channel status confirmatio",  IMGT_D_CHANNEL_STATUS_CO },
    { "REPORT_IN - REPORT indication",                       IMGT_REPORT_IN },
    { "STARTED_CO - management started",                     IMGT_STARTED_CO },
    { "STOPPED_CO - management stopped",                     IMGT_STOPPED_CO },
    { "D_CHANNEL_REL_RQ - D-channel Release Request",        IMGT_D_CHANNEL_REL_RQ },
    { "D_CHANNEL_REL_CO - D-channel Release Confirmation",   IMGT_D_CHANNEL_REL_CO },
    { "D_CHANNEL_EST_RQ - D-channel Establish Request",      IMGT_D_CHANNEL_EST_RQ },
    { "D_CHANNEL_EST_CO - D-channel Establish Confirmation", IMGT_D_CHANNEL_EST_CO },
    { NULL, 0}
};

void show_management_event(CTAHD ctahd, IMGT_MSG_PACKET *pkt)
{
    if (pkt == NULL) return;

    char text[200];
    char *text_ptr = text;
    size_t text_free = sizeof(text);
    char cta_text[100];

    IMGT_MESSAGE *msg = (IMGT_MESSAGE*)&pkt->message;

    switch (msg->code) {
        case IMGT_STARTED_CO:
        case IMGT_STOPPED_CO:
            ctaGetText(ctahd, msg->status, cta_text, sizeof(cta_text));
            ast_build_string(&text_ptr, &text_free, "status = %s (0x%lx)", cta_text, msg->status);
            break;

        case IMGT_REPORT_IN:
            {
            struct imgt_report_hdr *rpt = (struct imgt_report_hdr*)pkt->databuff;
            struct imgt_report_CallTag *call_tag = (struct imgt_report_CallTag*)pkt->databuff;
            switch (rpt->OperationID) {
                case IMGT_OP_ID_SET_CALL_TAG:
                    ast_build_string(&text_ptr, &text_free, "operation = Set Call Tag; slot = %d; call tag = %x",
                        call_tag->Slot, (unsigned int)call_tag->CallTag);
                    break;

                case IMGT_OP_ID_TRFD_CALL_CLEARING:
                    ast_build_string(&text_ptr, &text_free, "operation = Transferred Call Clearing; call tag = %x",
                        (unsigned int)call_tag->CallTag);
                    break;

                default:
                    ast_build_string(&text_ptr, &text_free, "unknown operation = %c", rpt->OperationID);
                    break;
            }
            }
            break;

        case IMGT_RESTART_IN:
            {
            struct imgt_restart *rst = (struct imgt_restart*)pkt->databuff;
            ast_build_string(&text_ptr, &text_free, "type = %s; nai = %d",
                (rst->type == PREFERENCE_TRUNK ) ? "Trunk" : (rst->type == PREFERENCE_BCHANNEL) ? "B-channel" : "Unknown",
                rst->nai);
            if (rst->type == PREFERENCE_BCHANNEL )
                ast_build_string(&text_ptr, &text_free, "; B-channel = %d", rst->BChannel);
            /* For some variants (DMS) the RESTART message resets
               the B channel status to "in service".  Every time
               when restart comes up check the status of the channel * /
            for ( cx_nb = 0; cx_nb < NumberOfNFASTrunks; cx_nb++ )
                if ( cx_array[cx_nb].nai == pimgt_rst->nai )
                    break;
            if ( cx_nb < NumberOfNFASTrunks )
            {
                if ( pimgt_rst->type == PREFERENCE_BCHANNEL )
                    QueryBChannelStatus( &cx_array[cx_nb], PREFERENCE_BCHANNEL,
                                        pimgt_rst->BChannel );
                else
                    QueryBChannelStatus( &cx_array[cx_nb], PREFERENCE_TRUNK, 0 );
            }
            else
            {
                printf( "Received Invalid NAI\n" );
            }
            */
            }
            break;

        case IMGT_SERVICE_IN:
        case IMGT_SERVICE_CO:
        case IMGT_B_CHANNEL_STATUS_CO:
            {
            struct imgt_service *svc = (struct imgt_service*)pkt->databuff;
            ast_build_string(&text_ptr, &text_free, "type = %s; nai = %d",
                (svc->type == PREFERENCE_TRUNK ) ? "Trunk" : (svc->type == PREFERENCE_BCHANNEL) ? "B-channel" : "Unknown",
                svc->nai);
            if (svc->type == PREFERENCE_BCHANNEL )
                ast_build_string(&text_ptr, &text_free, "; B-channel = %d", svc->BChannel);
            ast_build_string(&text_ptr, &text_free, "; status = %s",
                  (svc->status == IMGT_IN_SERVICE ) ?  "In Service" :
                  (svc->status == IMGT_MAINTENANCE ) ?  "Maintenance" :
                  (svc->status == IMGT_OUT_OF_SERVICE ) ? "Out Of Service" : "Unknown");
            /*
            for ( cx_nb = 0; cx_nb < NumberOfNFASTrunks; cx_nb++ )
                if ( cx_array[cx_nb].nai == pimgt_svc->nai )
                    break;
            if ( cx_nb < NumberOfNFASTrunks )
            {
                if( pimgt_svc->type == PREFERENCE_TRUNK )
                {
                    for ( i = 1; i <= channel_number; i++ )
                        cx_array[cx_nb].channel_status[i] = pimgt_svc->status;
                }
                else
                    cx_array[cx_nb].channel_status[pimgt_svc->BChannel] = pimgt_svc->status;
            }
            else
            {
                printf( "Received Invalid Nai\n" );
            }
            */
            }
            break;

        case IMGT_D_CHANNEL_STATUS_IN:
        case IMGT_D_CHANNEL_STATUS_CO:
            {
            struct imgt_d_channel_status *dchl_status = (struct imgt_d_channel_status*)pkt->databuff;

            ast_build_string(&text_ptr, &text_free, "nai = %d; D-channel status = %s", msg->nai,
                (dchl_status->status == IMGT_D_CHANNEL_STATUS_RELEASED) ? "Released" :
                (dchl_status->status == IMGT_D_CHANNEL_STATUS_ESTABLISHED) ? "Established" : "Awaiting Establishment");
            /*
            d_channel_status = pimgt_dchl_status->status;

            printf( "On Board %d NAI %d D-channel status - %s \n",
                cx_array[cx_d_channel].board, msg->nai,
                (d_channel_status == IMGT_D_CHANNEL_STATUS_RELEASED) ? "released" :
                (d_channel_status == IMGT_D_CHANNEL_STATUS_ESTABLISHED) ?
                    "established" : "awaiting establishment" );
            */
            }
            break;

        case IMGT_D_CHANNEL_EST_CO:
        case IMGT_D_CHANNEL_REL_CO:
            {
            struct imgt_d_channel_control *dchl_control = (struct imgt_d_channel_control*)pkt->databuff;

            ast_build_string(&text_ptr, &text_free, "nai = %d; D-channel %s request status = %d",
                msg->nai, (msg->code == IMGT_D_CHANNEL_EST_CO) ? "establish" : "release", (int)dchl_control->result);
            /*
            printf( "On Board %d NAI %d D-channel Establish Request Status - %d \n",
                cx_array[cx_d_channel].board, msg->nai, op_status);
            */
            }
            break;

        default:
            ast_build_string(&text_ptr, &text_free, "unknown code = %c", msg->code);
            break;
    }

    ast_verbose(VERBOSE_PREFIX_3 "chan_nms: mgmt: %s: %s\n", option_name(imgt_messages, msg->code), text);
    imgtReleaseBuffer(ctahd, pkt);
}

void show_event(CTA_EVENT *eventp)
{
    if (option_verbose <= 2) return;
    char format_buffer[100] = "";
    if (CTA_IS_USER_EVENT(eventp->id))
        snprintf(format_buffer, sizeof(format_buffer),
            "application event: 0x%08x; value: 0x%08x\n",
            (unsigned int)eventp->id, (unsigned int)eventp->value);
    else
        ctaFormatEvent(NULL, eventp, format_buffer, sizeof(format_buffer));
    ast_verbose(VERBOSE_PREFIX_3 "chan_nms: ctahd: 0x%08x; userid: %d; %s",
        (unsigned int)eventp->ctahd, (int)eventp->userid, format_buffer);
    if (eventp->id == IMGTEVN_RCV_MESSAGE)
        show_management_event(eventp->ctahd, (IMGT_MSG_PACKET*)eventp->buffer);
    return;
}

void wait_event_q_timeout(CTAQUEUEHD ctaqueue_hd, CTA_EVENT *eventp, int timeout)
{
    DWORD ret;
    while ((ret = ctaWaitEvent(ctaqueue_hd, eventp, timeout)) != SUCCESS) {
        ast_log(LOG_ERROR, "ctaWaitEvent() failed: 0x%x\n", (unsigned int)ret);
        usleep(1000);
    }
    if (eventp->id != CTAEVN_WAIT_TIMEOUT &&
        eventp->id != ADIEVN_RECORD_BUFFER_FULL &&
        eventp->id != ADIEVN_PLAY_BUFFER_REQ) /* high rate events */
        show_event(eventp);
    return;
}
/*
void wait_event_q(CTAQUEUEHD ctaqueue_hd, CTA_EVENT *eventp)
{
    wait_event_q_timeout(ctaqueue_hd, eventp, CTA_WAIT_FOREVER);
    return;
}
*/
void wait_event(CTAHD ctahd, CTA_EVENT *eventp)
{
    CTAQUEUEHD ctaqueue_hd;
    ctaGetQueueHandle(ctahd, &ctaqueue_hd);
    wait_event_q_timeout(ctaqueue_hd, eventp, 5000); /* 5sec should be enough */
    return;
}

void wait_specific_event(CTAHD ctahd, DWORD desired_event, CTA_EVENT *eventp)
{
    while (1) {
        wait_event(ctahd, eventp);
        if ((ctahd == eventp->ctahd) && (eventp->id == desired_event))
            break;
        if (eventp->id == CTAEVN_WAIT_TIMEOUT) {
            char format_buffer[100] = "";
            ctaGetText(ctahd, desired_event, format_buffer, sizeof(format_buffer));
            warn2p("timeout waiting for %s", format_buffer);
            break; // XXX return value
        }
        info3p("unexpected event, want: ctahd: 0x%08x, event id: 0x%08x", (unsigned int)ctahd, (unsigned int)desired_event);
    }
    return;
}

void free_event_buffer(CTA_EVENT *eventp)
{
    if (eventp->buffer && (eventp->size & CTA_INTERNAL_BUFFER))
        ctaFreeBuffer(eventp->buffer);
}

int open_context(CTAQUEUEHD ctaqueue_hd, int userid, char *contextname,
    CTAHD *ctahd_ptr, CTA_SERVICE_DESC *services, int num_services)
{
    DWORD ret;
    CTA_EVENT event = { 0 };
    if ((ret = ctaCreateContext(ctaqueue_hd, userid, contextname, ctahd_ptr)) != SUCCESS) {
        ast_log(LOG_ERROR, "ctaCreateContext() failed: 0x%x\n", (unsigned int)ret);
        return -1;
    }
    //ctaSetTraceLevel(*ctahd_ptr, "CTA", CTA_TRACEMASK_ALL);
    if ((ret = ctaOpenServices(*ctahd_ptr, services, num_services)) != SUCCESS) {
        ast_log(LOG_ERROR, "ctaOpenServices() failed: 0x%x\n", (unsigned int)ret);
        return -1;
    }
    wait_specific_event(*ctahd_ptr, CTAEVN_OPEN_SERVICES_DONE, &event);
    free_event_buffer(&event);
    if (event.value != CTA_REASON_FINISHED) {
        ast_log(LOG_ERROR, "ctaOpenServices() failed: 0x%x\n", (unsigned int)event.value);
        return -1;
    }
    return 0;
}

int start_trunk(CTAQUEUEHD ctaqueue_hd, int userid, char *contextname,
    CTAHD *ctahd_ptr, CTA_SERVICE_DESC *services, int num_services, int trunk, int trunk_mode)
{
    CTA_EVENT event = { 0 };

    if (open_context(ctaqueue_hd, userid, contextname, ctahd_ptr, services, num_services) == -1)
        return -1;

    isdnStartProtocol(*ctahd_ptr, ISDN_PROTOCOL_CHANNELIZED, operator, country, trunk_mode, trunk, NULL);
    wait_specific_event(*ctahd_ptr, ISDNEVN_START_PROTOCOL, &event);
    free_event_buffer(&event);
    if (event.value != SUCCESS) {
        ast_log(LOG_ERROR, "isdnStartProtocol() failed: 0x%x\n", (unsigned int)event.value);
        return -1;
    }
    info3p("trunk %d started; partner mode %s", trunk, option_name(option_partner_equipment_modes, trunk_mode));
    if (num_timeslots_per_trunk == T1 && send_maint_messages) { // XXX check for 5ESS or individual B-channel restarts
        IMGT_CONFIG config = {
            .imgt_mask = IMGT_SERVICE_MASK | IMGT_RESTART_MASK | IMGT_D_CHANNEL_STATUS_MASK,
            .mon_mask = IMGT_REPORT_MASK
        };
        DWORD ret;
        IMGT_MESSAGE msg_dest = { .nai = trunk, .code = IMGT_SERVICE_RQ, .nfas_group = 0 };
        struct imgt_service msg = { .type = PREFERENCE_TRUNK, .nai = trunk, .BChannel = 0, .status = IMGT_IN_SERVICE };
        if ((ret = imgtStart(*ctahd_ptr, trunk, &config)) != SUCCESS) {
            ast_log(LOG_ERROR, "imgtStart() failed: 0x%x\n", (unsigned int)ret);
            return -1;
        }
        wait_specific_event(*ctahd_ptr, IMGTEVN_RCV_MESSAGE, &event);
        free_event_buffer(&event);
        if (event.value != SUCCESS) {
            ast_log(LOG_ERROR, "imgtStart() failed: 0x%x\n", (unsigned int)event.value);
            return -1;
        }
        if ((ret = imgtSendMessage(*ctahd_ptr, &msg_dest, sizeof(msg), &msg)) != SUCCESS) {
            ast_log(LOG_ERROR, "imgtSendMessage() failed: 0x%x\n", (unsigned int)ret);
            return -1;
        }/*
        wait_specific_event(*ctahd_ptr, IMGTEVN_SEND_MESSAGE, &event);
        free_event_buffer(&event);
        if (event.value != SUCCESS) {
            ast_log(LOG_ERROR, "imgtStartSession() failed: 0x%x\n", (unsigned int)event.value);
            return -1;
        }*/
    }
    return 0;
}

int open_timeslot(CTAQUEUEHD ctaqueue_hd, int userid, char *contextname,
    CTAHD *ctahd_ptr, CTA_SERVICE_DESC *services, int num_services, int timeslot)
{
    CTA_EVENT event = { 0 };

    if (open_context(ctaqueue_hd, userid, contextname,ctahd_ptr, services, num_services) == -1)
        return -1;

    NCC_START_PARMS     nccparms;
    NCC_ADI_ISDN_PARMS  isdnparms;
    //NCC_ADI_START_PARMS adiparms;
    //NCC_ISDN_EXT_CALL_STATUS isdstatus;
    ctaGetParms(*ctahd_ptr, NCC_START_PARMID, &nccparms, sizeof(NCC_START_PARMS));
    nccparms.eventmask =
        NCC_REPORT_ALERTING   |
        NCC_REPORT_ANSWERED   |
        NCC_REPORT_STATUSINFO;

    ctaGetParms(*ctahd_ptr, NCC_ADI_ISDN_PARMID, &isdnparms, sizeof(NCC_ADI_ISDN_PARMS));
    isdnparms.isdn_start.ISDNeventmask |= ISDN_REPORT_PROGRESS;
    if (accept_alerting) {
        isdnparms.isdn_start.flags &= ~(PROGRESS_MASK_ACCEPT /*| PROGRESS_MASK_REJECT*/);
        isdnparms.isdn_start.flags |= (ALERTING_MASK_ACCEPT /*| ALERTING_MASK_REJECT*/);
    }
    isdnparms.isdn_start.flags |= ISDN_REPORT_PROGRESS;
    //isdnparms.isdn_start.exclusive = 0;

    nccStartProtocol(*ctahd_ptr, "isd0", &nccparms, NULL, &isdnparms.isdn_start);
    wait_specific_event(*ctahd_ptr, NCCEVN_START_PROTOCOL_DONE, &event);
    free_event_buffer(&event);
    if (event.value != CTA_REASON_FINISHED) {
        ast_log(LOG_ERROR, "ctaOpenServices() failed: 0x%x\n", (unsigned int)event.value);
        return -1;
    }
    debug2p("timeslot %d opened", timeslot);
    return 0;
}

// XXX handle NFAS setups
static int nms_init(void)
{
    DWORD ret;

    if (num_timeslots_per_trunk == E1) /* EuroISDN does not have IMGT function */
        num_services = 4;
    else
        num_services = 5;

    if ((ret = ctaInitialize(servicelist, num_services, &initparms)) != SUCCESS) {
        ast_log(LOG_ERROR, "ctaInitialize() failed: 0x%x\n", (unsigned int)ret);
        return -1;
    }
    ctaSetErrorHandler(err_handler, NULL);
    ctaCreateQueue(NULL, 0, &trunks_q);
    ctaCreateQueue(NULL, 0, &timeslots_q);

    trunks_hd = malloc(num_trunks * sizeof(trunks_hd));
    timeslots_hd = malloc(num_timeslots * sizeof(timeslots_hd));

    int i, trunk;
    char context_name[16];

    if (start_trunks) {
        for (i = 0; i < num_services; ++i) {
            services[i].mvipaddr.board  = board_no;
            services[i].mvipaddr.mode   = 0;
        }
        for (trunk = 0; trunk < num_trunks; ++trunk) {
            sprintf(context_name, "trunk%d", trunk);
            if (start_trunk(trunks_q, TRUNKS_USERID_OFFSET+trunk, context_name, &trunks_hd[trunk], services, num_services, trunk, partner_equipment_modes[trunk]) == -1)
                return -1;
            ++trunks_in_service;
        }
    }
    info("trunks ready");
    for (i = 0; i < num_services; ++i) {
        services[i].mvipaddr.board  = board_no;
        services[i].mvipaddr.stream = mvip_stream;
        services[i].mvipaddr.bus    = MVIP95_LOCAL_BUS;
        services[i].mvipaddr.mode   = ADI_FULL_DUPLEX;
    }
    int trunk_timeslot, timeslot, mvipaddr_timeslot, d_offset = 0;
    if (num_timeslots_per_trunk == T1) // XXX in T1 mode, D-channel is present in mvipaddr.timeslot, but not in E1 mode..?
        d_offset = 1;
    for (trunk = 0; trunk < num_trunks; ++trunk) {
        for (trunk_timeslot = 0; trunk_timeslot < num_timeslots_per_trunk; ++trunk_timeslot) {
            timeslot = num_timeslots_per_trunk * trunk + trunk_timeslot;
            mvipaddr_timeslot = (num_timeslots_per_trunk + d_offset) * trunk + trunk_timeslot;
            for (i = 0; i < num_services; ++i)
                services[i].mvipaddr.timeslot = mvipaddr_timeslot;
            sprintf(context_name, "timeslot%d", timeslot);
            /* timeslot is used as userid */
            if (open_timeslot(timeslots_q, timeslot, context_name, &timeslots_hd[timeslot], services, num_services, timeslot) == -1)
                return -1;
        }
    }

    return 0;
}

static void nms_stop(void)
{
    int i, trunks = trunks_in_service;
    if (trunks == 0)
        return;
    info2p("shutting down %d trunks, this may take a while", trunks);
    for (i = 0; i < trunks; ++i) {
        if (num_timeslots_per_trunk == T1 && send_maint_messages)
            imgtStop(trunks_hd[i]);
        // XXX check why NMS board sometimes is stuck on protocol shutdown (ISDNERR_RACE_STARTING_PROTOCOL)
        // when testing via loop
        // XXX check why delay doesn't work
        //const struct timespec delay = { .tv_sec = 10 };
        //nanosleep(&delay, NULL);
        isdnStopProtocol(trunks_hd[i]);
    }
}
