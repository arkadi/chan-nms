#!/bin/sh

m=chan_nms
t=.
#i=.
#i=/home/arkadi/tmp/asterisk-1.2.28/include
#i=/home/arkadi/tmp/asterisk-1.4.24.1/include
#i=/opt/asterisk-1.2/include
i=/opt/asterisk-1.4/include
#i=/opt/asterisk-1.6/include

gcc -I$i \
  -pipe -Wall -g -O -march=i686 -fPIC \
  -D_REENTRANT -D_GNU_SOURCE -DHAVE_LINUX_COMPILER_H -DCRYPTO \
  -c -o $t/$m.o $m.c \
  -DLINUX -DUNIX -DMULTITHREAD -DCT_ACCESS -I/opt/nms/include &&
gcc -shared -Xlinker -x -o $t/$m.so $t/$m.o
# -L/opt/nms/lib -Wl,-rpath,/opt/nms/lib -lcta -ladiapi -lnccapi -lvceapi -lswiapi -ladidtm -lisdnapi -lstdc++
